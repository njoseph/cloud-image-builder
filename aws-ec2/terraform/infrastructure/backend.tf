terraform {
  backend "s3" {
    bucket         = "freedombox-terraform-state-infrastructure"
    region         = "eu-central-1"
    key            = "infrastructure.json"
    dynamodb_table = "terraform-state-lock-infrastructure"
    encrypt        = true
  }
}

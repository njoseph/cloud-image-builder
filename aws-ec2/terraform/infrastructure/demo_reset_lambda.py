"""
Script to reset the server running at https://demo.freedombox.org
"""

import boto3


def get_elastic_ip_address(client):
    # Assumes that there's only one elastic IP address
    filters = [{'Name': 'tag:Name', 'Values': ['demo-server-ip']}]
    return client.describe_addresses(
        Filters=filters)['Addresses'][0]['PublicIp']


def terminate_stale_instances(client, eip):
    """Terminate inactive demo instances."""
    filters = [{
        'Name': 'instance-state-name',
        'Values': ['running']
    }, {
        'Name': 'tag:Name',
        'Values': ['demo-instance']
    }]
    reservations = client.describe_instances(Filters=filters)['Reservations']
    running_instances = [
        instance
        for instances in list(map(lambda r: r['Instances'], reservations))
        for instance in instances
    ]
    stale_instances = filter(
        lambda instance: instance['PublicIpAddress'] != eip, running_instances)
    stale_instance_ids = list(
        map(lambda instance: instance['InstanceId'], stale_instances))
    if stale_instance_ids:
        client.terminate_instances(InstanceIds=stale_instance_ids)


def get_demo_ami_id(client):
    filters = [{'Name': 'name', 'Values': ['demo-server']}]
    return client.describe_images(Owners=['self'],
                                  Filters=filters)['Images'][0]['ImageId']


def launch_new_demo_server(client, demo_ami_id):
    response = client.run_instances(
        ImageId=demo_ami_id, InstanceType='t3a.micro', MaxCount=1, MinCount=1,
        SecurityGroups=['All-TCP'], TagSpecifications=[{
            'ResourceType': 'instance',
            'Tags': [{
                'Key': 'Name',
                'Value': 'demo-instance'
            }]
        }], InstanceInitiatedShutdownBehavior='terminate')
    instance_id = response['Instances'][0]['InstanceId']
    instance = boto3.resource('ec2').Instance(instance_id)
    instance.wait_until_running()
    return instance_id


def associate_eip_to_instance(client, eip, instance_id):
    client.associate_address(InstanceId=instance_id, PublicIp=eip,
                             AllowReassociation=True)


def handler(event, _):
    client = boto3.client('ec2', 'eu-central-1')
    eip = get_elastic_ip_address(client)
    demo_ami_id = get_demo_ami_id(client)

    print(
        'Terminating old demo instances lingering from past failed runs of this Lambda'
    )
    terminate_stale_instances(client, eip)

    print('Launching new demo instance')
    instance_id = launch_new_demo_server(client, demo_ami_id)
    print('New demo instance id', instance_id)
    associate_eip_to_instance(client, eip, instance_id)

    print('Terminating the previous demo instance')
    terminate_stale_instances(client, eip)


if __name__ == '__main__':
    handler(None, None)

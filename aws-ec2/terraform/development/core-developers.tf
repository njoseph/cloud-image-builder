###########################################################
# Create core developers groups with administrator access #
###########################################################

resource "aws_iam_group" "core_developers" {
  name = "CoreDevelopers"
}

resource "aws_iam_group_policy_attachment" "administrator_access_to_core_developers" {
  group      = aws_iam_group.core_developers.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}


############################################
# Create users for current core developers #
############################################

resource "aws_iam_user" "njoseph" {
  name = "njoseph"
  path = "/coredeveloper/"
  tags = {
    Name = "Joseph Nuthalapati"
  }
}

resource "aws_iam_user" "jvalleroy" {
  name = "jvalleroy"
  path = "/coredeveloper/"
  tags = {
    Name = "James Valleroy"
  }
}

resource "aws_iam_user" "sunil" {
  name = "sunil"
  path = "/coredeveloper/"
  tags = {
    Name = "Sunil Mohan Adapa"
  }
}

#########################################
# Add core developer users to the group #
#########################################

resource aws_iam_user_group_membership "njoseph_core_developer" {
  user = aws_iam_user.njoseph.name

  groups = [
    aws_iam_group.core_developers.name
  ]
}

resource aws_iam_user_group_membership "jvalleroy_core_developer" {
  user = aws_iam_user.jvalleroy.name

  groups = [
    aws_iam_group.core_developers.name
  ]
}

resource aws_iam_user_group_membership "sunil_core_developer" {
  user = aws_iam_user.sunil.name

  groups = [
    aws_iam_group.core_developers.name
  ]
}

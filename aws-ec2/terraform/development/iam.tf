resource "aws_iam_policy" "packer" {
  description = "Policy to let an EC2 instance become a Packer builder"
  name        = "PackerEC2Builder"
  path        = "/"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "ec2:AttachVolume",
            "ec2:AuthorizeSecurityGroupIngress",
            "ec2:CopyImage",
            "ec2:CreateImage",
            "ec2:CreateKeypair",
            "ec2:CreateSecurityGroup",
            "ec2:CreateSnapshot",
            "ec2:CreateTags",
            "ec2:CreateVolume",
            "ec2:DeleteKeyPair",
            "ec2:DeleteSecurityGroup",
            "ec2:DeleteSnapshot",
            "ec2:DeleteVolume",
            "ec2:DeregisterImage",
            "ec2:DescribeImageAttribute",
            "ec2:DescribeImages",
            "ec2:DescribeInstances",
            "ec2:DescribeInstanceStatus",
            "ec2:DescribeRegions",
            "ec2:DescribeSecurityGroups",
            "ec2:DescribeSnapshots",
            "ec2:DescribeSubnets",
            "ec2:DescribeTags",
            "ec2:DescribeVolumes",
            "ec2:DetachVolume",
            "ec2:GetPasswordData",
            "ec2:ModifyImageAttribute",
            "ec2:ModifyInstanceAttribute",
            "ec2:ModifySnapshotAttribute",
            "ec2:RegisterImage",
            "ec2:RunInstances",
            "ec2:StopInstances",
            "ec2:TerminateInstances"
          ],
          "Resource" : "*"
        }
      ]
  })
}

resource "aws_iam_policy" "ansible" {
  description = "Permissions required for Ansible scripts to cleanup AMIs, reset the demo server etc."
  name        = "AnsibleEC2Operations"
  path        = "/"
  policy = jsonencode(
    {
      Statement = [
        {
          Action = [
            "ec2:DescribeImages",
            "ec2:DisassociateAddress",
            "ec2:DeregisterImage",
            "ec2:DescribeAddresses",
            "ec2:DescribeImageAttribute",
            "ec2:AssociateAddress",
          ]
          Effect   = "Allow"
          Resource = "*"
          Sid      = "VisualEditor0"
        },
      ]
      Version = "2012-10-17"
    }
  )
}

resource "aws_iam_policy" "ami-cleanup" {
  description = "Permissions to list AMIs and delete them"
  name        = "AMICleanup"
  path        = "/"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "ec2:DescribeImages",
            "ec2:DeregisterImage"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
}

resource "aws_iam_role" "developer" {
  assume_role_policy = jsonencode(
    {
      Statement = [
        {
          Action = "sts:AssumeRole"
          Effect = "Allow"
          Principal = {
            Service = "ec2.amazonaws.com"
          }
        },
      ]
      Version = "2012-10-17"
    }
  )
  description           = "Allow developers to run commands on an EC2 instance without going through the SAML authentication process."
  force_detach_policies = false
  max_session_duration  = 3600
  name                  = "Developer"
  path                  = "/"
  tags = {
    "Name" = "Developer"
  }
}

resource "aws_iam_role_policy_attachment" "packer-attachment" {
  role       = aws_iam_role.developer.name
  policy_arn = aws_iam_policy.packer.arn
}

resource "aws_iam_role_policy_attachment" "ansible-attachment" {
  role       = aws_iam_role.developer.name
  policy_arn = aws_iam_policy.ansible.arn
}

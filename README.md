# FreedomBox Cloud Image Builder

Build images of FreedomBox for various cloud providers.

## Supported cloud service providers:
- AWS EC2

## Installation:

Requirements:
- Hashicorp Packer
- Ansible and its dependencies

### Setup

1. Add the provided JSON policy files into your AWS account's IAM
2. Create an IAM role, say Developer and associate the above policies to it
3. Create an EC2 instance for development and associate the above IAM role with it.

#### Development instance setup

Install Ansible and libraries for AWS EC2 on the machine which executes these playbooks.

```shell
sudo apt -y install python3-venv
python3 -m venv ~/.virtualenvs/cloud
source ~/.virtualenvs/cloud/bin/activate
pip install ansible boto3 boto
```

Get the the officially published Packer binary for GNU/Linux and copy it to /usr/local/bin/.

## Usage:

Run the following command to generate a new FreedomBox AMI.

```shell
packer build packers/freedombox/freedombox-packer.json
```

**Note:** The AMI will be automatically copied to multiple AWS regions.

## Ansible Roles

Activate the virtual environment before running any ansible-playbook command

```shell
source ~/.virtualenvs/cloud/bin/activate
```

### cleanup

This role cleans up any left-over AMIs and snapshots running after creating the FreedomBox AMI. This role can be invoked by calling its corresponding playbook.

```shell
ansible-playbook cleanup.yml
```

### demo

This role replaces the FreedomBox demo server with a new one. This can be invoked as follows:

```shell
ansible-playbook demo-reset.yml
```

